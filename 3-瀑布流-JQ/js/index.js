let lazyImageModule = (function () {
    let $container = $('.container'),
        $columns = $container.children('.column');

    /* 1.获取到需要绑定的数据 */
    // 封装一个方法，基于AJAX“同步”的从服务器获取想要的数据
    const queryData = () => {
        let data = [];
        // JQ中提供一个封装好的方法$.ajax，可以让我们快速获取数据「原理和自己些的那四部是一样的」
        $.ajax({
            url: './data.json',
            async: false,
            dataType: 'json',
            success(result) {
                // result就是从服务器获取的数据
                data = result;
            }
        });
        return data;
    };

    /* 2.实现数据绑定「瀑布流」 */
    // 封装一个方法，传递进来数据，动态创建CARD，插入到指定的列中
    const bindHTML = data => {
        // 从服务器获取图片宽高「真实宽高」，在根据渲染宽度230，动态计算出渲染的高度
        data = data.map(item => {
            let {
                width,
                height
            } = item;
            item.width = 230;
            item.height = 230 / (width / height);
            return item;
        });

        // 每三个数据为一组进行遍历
        for (let i = 0; i < data.length; i += 3) {
            // 三条数据按照图片高度排序 小->大
            let group = data.slice(i, i + 3);
            group.sort((a, b) => a.height - b.height);

            // 三列按照盒子的高度排序 大->小
            //  + JQ方法获取的元素集合(类数组集合)可以使用SORT，因为JQ帮助我们实现了和数组相同的SORT方法
            $columns = $columns.sort((a, b) => b.offsetHeight - a.offsetHeight);

            // 遍历三条数据，动态创建CARD，并且插入到指定的列中
            group.forEach((item, index) => {
                // JQ中动态创建DOM元素:$([HTML字符串])
                let {
                    pic,
                    height,
                    title,
                    link
                } = item;
                $(`<div class="card">
                <a href="${link}">
                    <div class="lazyImageBox" style="height: ${height}px;">
                        <img src="" alt="" data-image="${pic}">
                    </div>
                    <p>${title}</p>
                </a>
            </div>`).appendTo($columns[index]);
            });
        }
    };

    /* 3.实现图片延迟加载 */
    // 封装一个方法，基于IntersectionObserver监听DOM的方案实现延迟加载
    const lazyImageFunc = () => {
        // 创建监听器
        let options = {
            threshold: [1]
        };
        let ob = new IntersectionObserver(changes => {
            changes.forEach(item => {
                let {
                    target,
                    isIntersecting
                } = item;
                if (isIntersecting) {
                    // 当前监听的盒子符合延迟加载条件后，我们进行延迟加载即可「处理过的盒子设置isLoad标记」
                    let $target = $(target),
                        $img = $target.find('img'),
                        dataImage = $img.attr('data-image');
                    $img.attr('src', dataImage);
                    $img.on('load', () => {
                        $img.css('opacity', 1);
                    });
                    $target.attr('isLoad', true);

                    // 加载过的移除监听
                    ob.unobserve(target);
                }
            });
        }, options);

        // 监听DOM元素：所有不具备isLoad标记的lazyImageBox盒子
        let $lazyImageBoxs = $container.find('.lazyImageBox:not([isLoad])');
        $lazyImageBoxs.each((index, item) => {
            ob.observe(item);
        });
    };

    /* 4.实现下拉加载更多数据 */
    // 封装一个方法，基于IntersectionObserver监听loadMore，出现在视口中，说明到达底部，加载更多数据
    const loadMoreFunc = () => {
        let $loadMore = $('.loadMore'),
            options = {
                threshold: [0]
            };
        let ob = new IntersectionObserver(changes => {
            let item = changes[0];
            if (item.isIntersecting) {
                // 加载更多数据
                let data = queryData();
                bindHTML(data);
                lazyImageFunc();
            }
        }, options);
        ob.observe($loadMore[0]);
    };

    return {
        // 入口：按照顺序依次执行对应的方法，实现出对应的需求
        init() {
            let data = queryData();
            bindHTML(data);
            lazyImageFunc();
            loadMoreFunc();
        }
    };
})();
lazyImageModule.init();