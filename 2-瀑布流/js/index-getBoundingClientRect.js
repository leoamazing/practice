function throttle(func, wait = 500) {
    let timer = null,
        previous = 0;
    return function anonymous(...params) {
        let now = new Date(),
            remaining = wait - (now - previous);
        if (remaining <= 0) {
            clearTimeout(timer);
            timer = null;
            previous = now;
            func.call(this, ...params);
        } else if (!timer) {
            timer = setTimeout(() => {
                clearTimeout(timer);
                timer = null;
                previous = new Date();
                func.call(this, ...params);
            }, remaining);
        }
    };
}

// 获取需要操作的DOM元素
let container = document.querySelector('.container'),
    columns = container.querySelectorAll('.column'),
    lazyImageBoxs = [];
// 把获取的类数组集合变为数组集合，这样以后就可以使用数组的办法了
columns = Array.from(columns);

// 基于AJAX从服务器端获取数据
// 注意：基于VSCODE预览的项目的时候，不能再使用OPEN IN BROWSER这种模式了，因为这种模式是file://协议，不支持AJAX的请求；我们可以安装Live Server插件，基于Open With Live Server来预览，这样就可以了！！这种模式会再本地创建一个Web服务 http://127.0.0.1:5500/，基于这个服务就可以访问本地的数据了！！
const queryData = () => {
    let data = [],
        xhr = new XMLHttpRequest;
    // false:同步获取数据「当前数据没有获取到之前，其余的事情是不进行处理的」，但是真实项目中都应该用异步获取，再基于promise/async/await等异步方案进行管理
    xhr.open('GET', './data.json', false);
    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4 && xhr.status === 200) {
            // xhr.responseText:获取的结果是一个JSON格式的字符串
            data = JSON.parse(xhr.responseText);
        }
    };
    xhr.send();
    return data;
};

// 根据获取的数据，完成页面中的数据绑定
const bindHTML = data => {
    // 页面渲染的时候，图片按照宽230px渲染，服务器返回的图片大小：宽300*高433，所以为了保证图片不变形，我们需要把获取的数据中的图片宽高，按照真实的比例进行缩放  
    // A->渲染  B->服务器  A-W/A-H=B-W/B-H => A-H=A-W/(B-W/B-H)
    data = data.map(item => {
        let AW = 230,
            BW = item.width,
            BH = item.height,
            AH = AW / (BW / BH);
        item.width = AW;
        item.height = AH;
        return item;
    });

    // 每三个为一组进行数据遍历
    for (let i = 0; i < data.length; i += 3) {
        // group:当前循环获取的三条数据&把数据按照图片的高度排序 小->大
        let group = data.slice(i, i + 3);
        group.sort((a, b) => a.height - b.height);

        // 把当前页面中的三列按照高度进行排序 大->小
        columns.sort((a, b) => b.offsetHeight - a.offsetHeight);

        // 按照排好序的结果，分别把数据插入到指定的列中
        group.forEach((item, index) => {
            // 根据当前数据动态创建一个CARD
            let card = document.createElement('div');
            card.className = 'card';
            card.innerHTML = `<a href="${item.link}">
                <div class="lazyImageBox" style="height: ${item.height}px;">
                    <img src="" alt="" data-image="${item.pic}">
                </div>
                <p>${item.title}</p>
            </a>`;

            // 创建的CARD追加到指定的列中
            columns[index].appendChild(card);
        });
    }

    // 数据绑定完成后，才可以获取到所有图片延迟加载所在的盒子
    lazyImageBoxs = Array.from(container.querySelectorAll('.lazyImageBox'));
};

// 图片的延迟加载
// 单张图片延迟加载
const lazyImgFunc = lazyImageBox => {
    let img = lazyImageBox.querySelector('img'),
        dataImage = img.getAttribute('data-image');
    img.onload = () => {
        img.style.opacity = 1;
    };
    img.src = dataImage;
    lazyImageBox.isLoad = true;
};
// 依次遍历每一个图片所在的盒子，所有符合条件的（出现在视口中的）都去做延迟加载
const lazyImageBoxsFunc = () => {
    let B = document.documentElement.clientHeight;
    lazyImageBoxs.forEach(lazyImageBox => {
        if (lazyImageBox.isLoad) return;
        let A = lazyImageBox.getBoundingClientRect().bottom;
        if (A <= B) {
            lazyImgFunc(lazyImageBox);
        }
    });
};

// 开始加载页面：获取数据、数据绑定、等待1000MS（再或者页面滚动的时候）做图片延迟加载
let data = queryData();
bindHTML(data);
setTimeout(lazyImageBoxsFunc, 500);
window.onscroll = throttle(() => {
    // 图片延迟加载
    lazyImageBoxsFunc();

    // 下拉加载更多数据
    let HTML = document.documentElement;
    if ((HTML.clientHeight + HTML.scrollTop + 100) >= HTML.scrollHeight) {
        let data = queryData();
        bindHTML(data);
    }
});