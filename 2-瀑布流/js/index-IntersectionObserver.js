let container = document.querySelector('.container'),
    columns = Array.from(container.querySelectorAll('.column')),
    loadMore = document.querySelector('.loadMore'),
    lazyImageBoxs = [];

// 基于AJAX从服务器端获取数据
const queryData = () => {
    let data = [],
        xhr = new XMLHttpRequest;
    xhr.open('GET', './data.json', false);
    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4 && xhr.status === 200) {
            data = JSON.parse(xhr.responseText);
        }
    };
    xhr.send();
    return data;
};

// 页面中的数据绑定
const bindHTML = data => {
    data = data.map(item => {
        let AW = 230,
            BW = item.width,
            BH = item.height,
            AH = AW / (BW / BH);
        item.width = AW;
        item.height = AH;
        return item;
    });
    for (let i = 0; i < data.length; i += 3) {
        let group = data.slice(i, i + 3);
        group.sort((a, b) => a.height - b.height);
        columns.sort((a, b) => b.offsetHeight - a.offsetHeight);
        group.forEach((item, index) => {
            let card = document.createElement('div');
            card.className = 'card';
            card.innerHTML = `<a href="${item.link}">
                <div class="lazyImageBox" style="height: ${item.height}px;">
                    <img src="" alt="" data-image="${item.pic}">
                </div>
                <p>${item.title}</p>
            </a>`;
            columns[index].appendChild(card);
        });
    }
};

// 单张图片的延迟加载
const lazyImgFunc = lazyImageBox => {
    let img = lazyImageBox.querySelector('img'),
        dataImage = img.getAttribute('data-image');
    img.onload = () => {
        img.style.opacity = 1;
    };
    img.src = dataImage;
    lazyImageBox.isLoad = true;
};

// 新方案处理图片的延迟加载
let options = {
    threshold: [1]
};
let obImage = new IntersectionObserver(changes => {
    // changes:数组集合，包含所有监听的DOM元素
    changes.forEach(item => {
        if (item.isIntersecting) {
            // 当前监听这一项符合条件，我们进行延迟加载
            lazyImgFunc(item.target);
            obImage.unobserve(item.target);
        }
    });
}, options);
const observeAllImageBox = () => {
    // 首先获取所有图片的盒子&移除已经做过延迟加载的
    lazyImageBoxs = Array.from(container.querySelectorAll('.lazyImageBox'));
    lazyImageBoxs = lazyImageBoxs.filter(item => !item.isLoad);
    lazyImageBoxs.forEach(lazyImageBox => {
        obImage.observe(lazyImageBox);
    });
};

// 下拉加载更多数据
let obLoadMore = new IntersectionObserver(changes => {
    let item = changes[0];
    if (item.isIntersecting) {
        // 加载更多数据
        let data = queryData();
        bindHTML(data);
        observeAllImageBox();
    }
}, {
    threshold: [0]
});
obLoadMore.observe(loadMore);

// 加载页面完成数据绑定和DOM监听
let data = queryData();
bindHTML(data);
observeAllImageBox();