/*
 * API配置说明
 *    new Banner3D([selector|element],[source],[config])
 *    + initial [number]初始展示索引,默认值0
 *    + speed [number]切换的速度,默认值500MS
 *    + autoplay [boolean]是否开启自动轮播,默认值true
 *    + interval [number]多长时间自动轮播一次,默认值3000MS
 *    + stop [boolean]是否开启鼠标进入容器暂定自动轮播、鼠标离开继续自动轮播「自动轮播模式下生效」,默认值true
 *    + navigation [boolean]是否开启左右导航切换,默认值true
 *    + clickable [boolean]是否开启点击slide切换,默认值true
 *    + on [object]周期函数
 *      + init(实例) 初始化完成触发的回调函数
 *      + transitionEnd(实例,索引) 每一次切换完触发的回调函数
 *    ----
 *    实例属性：
 *      + slides [array]所有的slide
 *      + activeIndex [number]当前展示的slide索引
 *    实例方法
 *      + runTo(索引) 执行这个方法,可以切换到指定索引的slide
 */
(function () {
    "use strict";

    // 默认配置项
    let initalConfig = {
        initial: {
            type: 'number',
            required: false,
            default: 0
        },
        speed: {
            type: 'number',
            default: 500
        },
        autoplay: {
            type: 'boolean',
            default: true
        },
        interval: {
            type: 'number',
            default: 3000
        },
        stop: {
            type: 'boolean',
            default: true
        },
        navigation: {
            type: 'boolean',
            default: true
        },
        clickable: {
            type: 'boolean',
            default: true
        },
        on: {
            type: 'object',
            default: {
                init() {},
                transitionEnd() {}
            }
        }
    };

    const checkSlide = target => {
        while (target) {
            let cn = target.className;
            if (typeof cn === 'string' && cn.includes('banner3D-slide')) return target;
            target = target.parentNode;
        }
        return false;
    };

    class Banner3D {
        constructor(selector, source, config) {
            let self = this;

            // 处理容器
            if (selector == null) throw new ReferenceError('selector must be required');
            self.container = selector.nodeType === 1 ? selector : document.querySelector(selector);
            if (self.container == null || self.container.nodeType !== 1) throw new TypeError('selector must be an element or effective selector');

            // 处理数据
            if (!Array.isArray(source)) throw new TypeError('source must be an array');
            self.source = source;

            // 处理配置项   
            if (!utils.isPlainObject(config)) config = {};
            utils.each(initalConfig, (rule, key) => {
                let {
                    type,
                    required,
                    default: defaultValue
                } = rule;
                let configValue = config[key],
                    configType = typeof configValue;
                // 规则要求必传，但是我们没有传:报错
                if (required && configValue === undefined) throw new TypeError(`${key} must be required`);
                if (configValue !== undefined) {
                    // 我们自己传递值了，此时我们需要校验规则中的类型是否符合
                    // 特殊：属性是on，要求传递的值必须是纯粹对象
                    if (key === 'on') {
                        if (!utils.isPlainObject(configValue)) throw new TypeError(`on must be an plain object`);
                        config[key] = Object.assign({}, defaultValue, configValue);
                    } else {
                        if (configType !== type) throw new TypeError(`${key} must be an ${type}`);
                        config[key] = configValue;
                    }
                } else {
                    // 自己没有传递值，则让config中这个配置项等于默认值
                    config[key] = defaultValue;
                }
            });
            self.config = config;
            self.activeIndex = config.initial;

            // 挂载一些其他的属性
            self.autoTimer = null;
            self.wrapper = self.container.querySelector('.banner3D-wrapper');
            if (self.wrapper == null || self.wrapper.nodeType !== 1) throw new TypeError('wrapper element must be existence');
            self.buttonPrev = self.container.querySelector('.button-prev');
            self.buttonNext = self.container.querySelector('.button-next');
            self.slides = [];

            // 开始渲染和相关的处理
            self.init();
        }
        init() {
            let self = this,
                {
                    source,
                    container,
                    buttonPrev,
                    buttonNext,
                    config: {
                        autoplay,
                        stop,
                        navigation,
                        clickable
                    }
                } = self;
            // 确保数据是五项
            while (source.length < 5) {
                let diff = 5 - source.length;
                source.slice(0, diff).forEach(item => {
                    source.push({
                        ...item,
                        id: parseInt(source[source.length - 1].id) + 1
                    });
                });
            }
            // 开始渲染(第一次渲染)
            self.render(true);
            // 开启自动轮播
            if (autoplay) self.autoMove();
            // 控制暂停和继续
            if (stop && autoplay) {
                container.addEventListener('mouseenter', () => clearInterval(self.autoTimer));
                container.addEventListener('mouseleave', () => self.autoMove());
            }
            // 基于事件委托实现点击操作
            if (navigation) {
                if (buttonPrev) buttonPrev.style.display = 'block';
                if (buttonNext) buttonNext.style.display = 'block';
            }
            container.addEventListener('click', utils.throttle(ev => {
                let target = ev.target;
                // 点击导航切换
                if (navigation) {
                    if (target === buttonNext) {
                        self.activeIndex++;
                        if (self.activeIndex > source.length - 1) self.activeIndex = 0;
                        self.render();
                        return;
                    }
                    if (target === buttonPrev) {
                        self.activeIndex--;
                        if (self.activeIndex < 0) self.activeIndex = source.length - 1;
                        self.render();
                        return;
                    }
                }
                // 点击slide
                if (clickable) {
                    target = checkSlide(target);
                    if (target) {
                        self.activeIndex = +target.getAttribute('data-index');
                        self.render();
                    }
                }
            }));
        }
        // 渲染数据 & 更新每个slide的样式
        render(inital) {
            let self = this,
                {
                    source,
                    activeIndex,
                    slides,
                    wrapper,
                    config: {
                        speed,
                        on: {
                            init,
                            transitionEnd
                        }
                    }
                } = self;

            let str = ``,
                len = source.length;
            let temp1 = activeIndex - 2,
                temp2 = activeIndex - 1,
                temp3 = activeIndex,
                temp4 = activeIndex + 1,
                temp5 = activeIndex + 2;
            if (temp1 < 0) temp1 = len + temp1;
            if (temp2 < 0) temp2 = len + temp2;
            if (temp4 > len - 1) temp4 = temp4 - len;
            if (temp5 > len - 1) temp5 = temp5 - len;
            source.forEach((item, index) => {
                let transform = 'translate(-50%, -50%) scale(0.55)',
                    zIndex = 0,
                    className = 'banner3D-slide';
                switch (index) {
                    case temp3:
                        transform = 'translate(-50%, -50%) scale(1)';
                        zIndex = 3;
                        className = 'banner3D-slide active';
                        break;
                    case temp1:
                        transform = 'translate(-195%, -50%) scale(0.7)';
                        zIndex = 1;
                        break;
                    case temp2:
                        transform = 'translate(-130%, -50%) scale(0.85)';
                        zIndex = 2;
                        break;
                    case temp4:
                        transform = 'translate(30%, -50%) scale(0.85)';
                        zIndex = 2;
                        break;
                    case temp5:
                        transform = 'translate(95%, -50%) scale(0.7)';
                        zIndex = 1;
                        break;
                }
                item.sty = `transform:${transform};z-index:${zIndex};`;
                item.className = className;
            });
            if (!inital) {
                source.forEach((item, index) => {
                    let cur = slides[index];
                    cur.style = item.sty;
                    cur.style.transitionDuration = `${speed}ms`;
                    cur.className = item.className;
                });
                // 每次更新完
                if (typeof transitionEnd === 'function') transitionEnd(self, self.activeIndex);
                return;
            }
            source.forEach((item, index) => {
                let {
                    pic,
                    className,
                    sty,
                    descript: {
                        name,
                        identity,
                        dream
                    }
                } = item;
                str += `<div class="${className}" style="${sty}" data-index="${index}">
                    <img src="${pic}" alt="" />
                    <div class="banner3D-mark"></div>
                    <p class="banner3D-detail">
                        <span>${name}</span>
                        <span>身份：${identity}</span>
                        <span>梦想：${dream}</span>
                    </p>
                </div>`;
            });
            wrapper.innerHTML = str;
            wrapper.style.display = 'block';
            self.slides = Array.from(wrapper.querySelectorAll('.banner3D-slide'));
            // 初始化完成
            if (typeof init === 'function') init(self);
        }
        // 自动轮播
        autoMove() {
            let self = this,
                {
                    config: {
                        interval
                    },
                    source
                } = self;
            self.autoTimer = setInterval(() => {
                self.activeIndex++;
                if (self.activeIndex > source.length - 1) self.activeIndex = 0;
                self.render();
            }, interval);
        }
        // 运动到指定项
        runTo(index) {
            let self = this,
                {
                    source
                } = self;
            let len = source.length;
            index = +index;
            if (isNaN(index)) throw new TypeError('index must be an number');
            index = index < 0 ? len + index : (index > len - 1 ? index - len : index);
            self.activeIndex = index;
            self.render();
        }
    }

    /* 暴露API */
    if (typeof window !== 'undefined') window.Banner3D = Banner3D;
    if (typeof module === 'object' && typeof module.exports === 'object') module.exports = Banner3D;
})();