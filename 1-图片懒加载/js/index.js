function throttle(func, wait = 500) {
    let timer = null,
        previous = 0;
    return function anonymous(...params) {
        let now = new Date(),
            remaining = wait - (now - previous);
        if (remaining <= 0) {
            clearTimeout(timer);
            timer = null;
            previous = now;
            func.call(this, ...params);
        } else if (!timer) {
            timer = setTimeout(() => {
                clearTimeout(timer);
                timer = null;
                previous = new Date();
                func.call(this, ...params);
            }, remaining);
        }
    };
}

function offset(element) {
    let l = element.offsetLeft,
        t = element.offsetTop,
        p = element.offsetParent;
    while (p && p.tagName !== 'body') {
        if (!/MSIE 8/.test(navigator.userAgent)) {
            l += p.clientLeft;
            t += p.clientTop;
        }
        l += p.offsetLeft;
        t += p.offsetTop;
        p = p.offsetParent;
    }
    return {
        top: t,
        left: l
    };
}