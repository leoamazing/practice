/* 
Semantic Versioning 是一个前端通用的版本定义规范。格式为："{MAJOR}.{MINOR}.{PATCH}-{alpha|beta|rc}.{number}", 要求实现compare(a, b)方法，比较ab两个版本的大小。
+ 当 a > b 返回 1;
+ 当 a = b 返回 0;
+ 当 a < b 返回 - 1;

其中 rc > beta > alpha, major > minor > patch;
例子：1.2.3 < 1.2.4 < 1.3.0.alpha.1 < 1.3.0.alpha.2 < 1.3.0.beta.1 < 1.3.0.rc.1 < 1.3.0 
*/

const compare = (a, b) => {
    let index = -1;
    let flag;
    a = a.split(/(?:\.|-)/g);  // ['1', '3', '0', 'alpha', '1']
    b = b.split(/(?:\.|-)/g);  // ['1', '3', '0', 'alpha', '2']
    const next = () => {
        index++;
        let a1 = a[index];
        let a2 = b[index];
        if (!a1 || !a2) {
            flag = (!a1 && !a2) ? 0 : (!a1 ? 1 : -1);
            return;
        }
        flag = (isNaN(a1) || isNaN(a1)) ? a1.localeCompare(a2) : a1 - a2;
        if (flag === 0) {
            next();
        }
    }
    next();
    return flag;
}

console.log(compare('1.3.0.alpha.1', '1.3.0.alpha.2')); // -1
console.log(compare('1.3.0.alpha.2', '1.3.0.alpha.1')); // 1
console.log(compare('1.3.0.alpha.1', '1.3.0.alpha.1')); // 0

/* 
* 扩展：
* 输入=>['1.1', '2.3.3', '4.3.5', '0.3.1', '0.302.1', '4.20.0', '4.3.5.1', '1.2.3.4.5']
* 输出=>['0.3.1', '0.302.1', '1.1', '1.2.3.4.5', '2.3.3', '4.3.5', '4.3.5.1', '4.20.0'] 
*/
const arr = ['1.1', '2.3.3', '4.3.5', '0.3.1', '0.302.1', '4.20.0', '4.3.5.1', '1.2.3.4.5']

/* 1. 字符串比较法(不准确) */
// arr.sort((a, b) => a > b ? 1 : -1)
// console.log(arr) // ['0.3.1', '0.302.1', '1.1', '1.2.3.4.5', '2.3.3', '4.20.0', '4.3.5', '4.3.5.1']

/* 2.“大数”加权法：原理=> MAJOR*p2 + MINOR*p + PATCH */
// const p = 1000;
// const maxLen = Math.max(...arr.map((item) => item.split('.').length));
// const gen = (val) => val.split('.').reduce(reducer, 0);
// const reducer = (pre, cur, index) => pre + (+cur) * Math.pow(p, maxLen - index - 1);
// arr.sort((a, b) => gen(a) > gen(b) ? 1 : -1);
// console.log(arr)// ['0.3.1', '0.302.1', '1.1', '1.2.3.4.5', '2.3.3', '4.3.5', '4.3.5.1', '4.20.0']

/* 3.循环比较法 */
arr.sort((a, b) => {
    let index = 0;
    const arr1 = a.split('.');
    const arr2 = b.split('.');
    while (true) {
        const s1 = arr1[index];
        const s2 = arr2[index++];
        if (s1 === undefined || s2 === undefined) {
            return arr1.length - arr2.length;
        }
        if (s1 === s2) continue;
        return s1 - s2;
    }
})
console.log(arr); // ['0.3.1', '0.302.1', '1.1', '1.2.3.4.5', '2.3.3', '4.3.5', '4.3.5.1', '4.20.0'] 
