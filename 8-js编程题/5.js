const data = [
    { id: 0, parentId: null, text: '第一级文字' },
    { id: 1, parentId: 0, text: '第二级文字' },
    { id: 2, parentId: 0, text: '第三级文字' },
    { id: 3, parentId: null, text: '第四级文字' },
    { id: 4, parentId: null, text: '第五级文字' },
    { id: 5, parentId: 3, text: '第六级文字' },
    { id: 6, parentId: 3, text: '第七级文字' },
]

/* 方法1：O(N^2) */
// const list2tree = (data) => {
//     const arr = data.filter(item => item.parentId === null);
//     arr.forEach(item => {
//         const children = data.filter(child => child.parentId === item.id);
//         if (children.length) item.children = children;
//     });
//     return arr;
// }
// console.log(list2tree(data));

// [
//     {
//         "id": 0,
//         "parentId": null,
//         "text": "第一级文字",
//         "children": [{ "id": 1, "parentId": 0, "text": "第二级文字" }, { "id": 2, "parentId": 0, "text": "第三级文字" }]
//     },
//     {
//         "id": 3,
//         "parentId": null,
//         "text": "第四级文字",
//         "children": [{ "id": 5, "parentId": 3, "text": "第六级文字" }, { "id": 6, "parentId": 3, "text": "第七级文字" }]
//     },
//     { "id": 4, "parentId": null, "text": "第五级文字" }
// ]


/* 方法2：O(N) */
const list2tree2 = (data) => {
    const result = [];
    const map = new Map();
    data.forEach(item => {
        map.set(item.id, item);
    })
    data.forEach(item => {
        let { parentId } = item,
            parent;
        if (parentId === null) {
            result.push(item);
            return
        }
        parent = map.get(item.parentId);
        parent.children ? parent.children.push(item) : parent.children = [item];
    })
    return result;
}
console.log(list2tree2(data));