/* 
* 编写computed方法实现
* console.log(computed(20876)); // '贰万零捌佰柒拾陆' 
*/

const computed = (num) => {
    const arr = ['零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖'];
    const arr2 = ['', '拾', '佰', '千', '万'];

    num = String(num).split('').reverse();

    num = num.map((item, index) => {
        let char = arr[item];
        if (index > 0 && char !== '零') {
            char = char + arr2[index];
        }
        return char;
    });

    return num.reverse().join('');
}

console.log(computed(20876)); // 贰万零捌佰柒拾陆