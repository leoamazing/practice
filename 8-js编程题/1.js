const list = [1, 2, 3];

const square = num => {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(num * num);
        }, 1000)
    })
}

list.forEach(async x => {
    const res = await square(x);
    console.log(res);
})


/*
* 1. 代码运行后会输出什么结果？请写出来
* 2. 如果希望每隔1s输出一个结果，应该怎么改？
*/


/* 方法1：递归 */
// let index = 0;
// const next = async () => {
//     if (index >= list.length) return;
//     let x = list[index++],
//         res;
//     res = await square(x);
//     console.log(res);
//     next();
// }
// next();


/*  方法2：for await of  */
// const next = async () => {
//     for await (let x of list) {
//         const res = await square(x);
//         console.log(res);
//     }
// }
// next()