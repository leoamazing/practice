/* 
实现多维对象的扁平化处理
处理后的结果
{
     1: 100,
    'a.b': 1,
    'a.c': 2,
    'a.d.e': 3,
    'a.d[2]': 200,
    'b[0]': 1,
    'b[1]': 2,
    'b[2].a': 3,
    'b[2].b': 4,
    'c': 1
} 
*/

const obj = {
    a: {
        b: 1,
        c: 2,
        d: { e: 3, 2: 200 },
    },
    b: [1, 2, { a: 3, b: 4 }],
    c: 1,
    1: 100
}

const isPlainObject = function isPlainObject(obj) {
    return Object.prototype.toString.call(obj) === '[object Object]';
};

const each = function each(obj, callback) {
    let item, keys, key;
    if (Array.isArray(obj)) {
        for (let i = 0; i < obj.length; i++) {
            item = obj[i];
            if (callback.call(item, item, i) === false) break;
        }
    } else {
        keys = Object.getOwnPropertyNames(obj);
        if (typeof Symbol !== "undefined") keys = keys.concat(Object.getOwnPropertySymbols(obj));
        for (let i = 0; i < keys.length; i++) {
            key = keys[i];
            item = obj[key];
            if (callback.call(item, item, key) === false) break;
        }
    }
    return obj;
};

const flatten = (obj) => {
    let result = {};
    const flat = (obj, attr) => {
        const isObject = isPlainObject(obj);
        const isArray = Array.isArray(obj);
        if (isObject || isArray) {
            each(obj, (value, key) => {
                let temp = isNaN(value) ? `.${key}` : `[${key}]`;
                flat(value, !attr ? key : attr + temp)
            })
            return;
        }
        result[attr] = obj;
    }
    flat(obj, '');
    return result;
}

console.log(flatten(obj), 'flatten'); // 多维转一维
