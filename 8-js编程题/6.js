/* 

现有一个接口：https://xxx.com.students, 每次请求只能返回10个学生的课程成绩，如下：
[
    { name: '张三', score: 99, time: '2021-12-22' },
    { name: '李四', score: 60, time: '2021-12-10' },
    { name: '王五', score: 77, time: '2021-11-08' },
    ...
]

该接口有一定概率请求失败，不可忽略， response status code 500, body为空

要求：
实现一个函数，总共需获得100个成绩大于90分，且时间再2021年12月03日之后的学生的课程成绩，并按成绩从大到小返回（可以直接使用axio或fetch）

提示：
浏览器最多可以有6个并行的网络请求
尽可能再更短的时间内，运行完成得到结果
尽可能用最少的请求次数 

*/

/* 数据请求 */
const source = axios.cancelToken.source();
const query = () => {
    return axios.post('https://xxx.com.students', null, { cancelToken: source.token }).then(response => response.data);
}

/* 核心逻辑 */
const fetchStudents = () => {
    return new Promise((resolve) => {
        let works = new Array(6).fill(null),
            values = [];
        works.forEach(() => {
            const next = async () => {
                if (values.length >= 100) {
                    source.cancel();
                    values = values.slice(0, 100).sort((a, b) => b.score - a.score);
                    resolve(values);
                    return;
                }
                try {
                    let value = await query();
                    value = value.filter(item => item.score > 90 && (new Date(item.time) - new Date('2021-12-03') > 0))
                    values = values.concat(value);
                } catch (_) { }
                next();
            };
            next();
        });
    });
}