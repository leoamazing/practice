/* 给出如下地址数据格式，实现函数 getNameById, 输入address和id, 输出id对应的name */

const address = [
    {
        id: 1,
        name: '北京市',
        children: [
            {
                id: 11,
                name: '海淀区',
                children: [
                    {
                        id: 111,
                        name: '中关村',
                    },
                ]
            },
            {

                id: 12,
                name: '朝阳区',
            }
        ]
    },
    {
        id: 2,
        name: '河北省',
    }
]

const getNameById = (address, id) => {
    let result = '';
    const next = (address) => {
        for (let index = 0; index < address.length; index++) {
            const item = address[index];
            if (item.id === id) {
                result = item.name;
                break;
            }
            if (Array.isArray(item.children)) next(item.children);
        }
    }
    next(address);
    return result;
}
console.log(getNameById(address, 2)); // '河北省'
console.log(getNameById(address, 111)); // '中关村'
console.log(getNameById(address, 32)); // ''