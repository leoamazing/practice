// 模拟数据请求
const delay = function delay(interval) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(interval);
        }, interval);
    });
};

// 任务列表:数组、数组中每一项是个函数，函数执行就是发送一个请求(返回promise实例)
let tasks = [() => {
    return delay(1000);
}, () => {
    return delay(1001);
}, () => {
    return delay(1002);
}, () => {
    return delay(1003);
}, () => {
    return delay(1004);
}, () => {
    return delay(1005);
}, () => {
    return delay(1006);
}];

class TaskQueue {
    constructor(tasks, limit, onComplete) {
        // 把信息挂载到实例上，方便在其它的方法中基于实例获取
        let self = this;
        self.tasks = tasks;
        self.limit = limit;
        self.onComplete = onComplete;
        self.queue = []; //存放任务的队列
        self.runing = 0; //记录正在运行的任务数量
        self.index = 0; //记录取出任务的索引
        self.values = []; //记录每个任务完成的结果
    }
    pushStack(task) {
        // 把任务存储到队列中
        let self = this;
        self.queue.push(task);
        self.next();
    }
    async next() {
        // 核心方法:根据runing控制哪些任务执行
        let self = this,
            { tasks, limit, onComplete, queue, runing, values, index } = self;
        // 如果运行的任务数小于并发限制，而且能够取出对应的任务:取出对应任务并且去发送
        if (runing < limit && index <= tasks.length - 1) {
            self.runing++;
            let prevIndex = index,
                task = queue[self.index++],
                value;
            try {
                value = await task();
                values[prevIndex] = value;
            } catch (err) {
                values[prevIndex] = null;
            }
            self.runing--;
            self.runing === 0 ? onComplete(values) : self.next();
        }
    }
}
const createRequest = function createRequest(tasks, limit, onComplete) {
    if (!Array.isArray(tasks)) throw new TypeError("tasks must be an array");
    if (typeof limit === "function") onComplete = limit;
    limit = +limit;
    if (isNaN(limit)) limit = 2;
    if (typeof onComplete !== "function") onComplete = Function.prototype;
    // 把任务列表中的任务，依次存放到任务队列中
    let TQ = new TaskQueue(tasks, limit, onComplete);
    tasks.forEach((task) => {
        TQ.pushStack(task);
    });
};

createRequest(tasks, (values) => {
    console.log(`所有请求都成功`, values);
});