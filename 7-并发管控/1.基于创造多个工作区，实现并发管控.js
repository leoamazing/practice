// 模拟数据请求
const delay = function delay(interval) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(interval);
        }, interval);
    });
};

// 任务列表:数组、数组中每一项是个函数，函数执行就是发送一个请求(返回promise实例)
let tasks = [() => {
    return delay(1000);
}, () => {
    return delay(1001);
}, () => {
    return delay(1002);
}, () => {
    return delay(1003);
}, () => {
    return delay(1004);
}, () => {
    return delay(1005);
}, () => {
    return delay(1006);
}];

/**
 * createRequest:实现并发管控
 * @param {Array} tasks 需要并发的任务列表(每项都是函数,函数执行发送请求,返回promise实例)
 * @param {Number} limit 需要限制并发的数量(默认值：2)
 * @returns {Promise} 返回一个promise实例,当所有任务都成功后,实例为fulfilled,值是每一个请求成功的结果
 */
const createRequest = function createRequest(tasks, limit) {
    //init params
    if (!Array.isArray(tasks)) throw new TypeError("tasks is not an array");
    if (isNaN(+limit)) limit = 2;
    limit = limit < 1 ? 1 : limit > tasks.length ? tasks.length : limit;

    //限制几个并发，就需要创造几个工作区
    let works = new Array(limit).fill(null),
        values = [],
        index = 0;
    works = works.map(() => {
        return new Promise((resolve) => {
            //去任务列表中获取一个任务，拿到工作区执行；当此任务执行完，继续去任务列表处拿任务...
            const next = async () => {
                let prevIndex = index,
                    task = tasks[index++],
                    value;
                if (typeof task === "undefined") {
                    //任务列表中已经没有任务了,当前工作区已经处理完成
                    resolve();
                    return;
                }
                try {
                    value = await task();
                    values[prevIndex] = value;
                } catch (_) {
                    values[prevIndex] = null;
                }
                next();
            };
            next();
        });
    });

    //所有工作区的promise都是成功态,则证明请求都发送完成了
    return Promise.all(works).then(() => values);
};

createRequest(tasks).then((values) => {
    console.log("请求都完成：", values);
    // 请求都完成： (7) [1000, 1001, 1002, 1003, 1004, 1005, 1006]
});