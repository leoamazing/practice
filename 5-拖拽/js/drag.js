let box = document.querySelector('#box');
const down = function down(ev) {
    this.style.cursor = 'move';
    this.startX = ev.pageX;
    this.startY = ev.pageY;
    this.startL = this.offsetLeft;
    this.startT = this.offsetTop;
    // DOM2的事件绑定要求：如果想后期移除事件绑定，那么绑定的方法不建议是匿名函数，因为后期移除的时候只有知道是哪个方法才可以移除绑定
    this._MOVE = move.bind(this);
    this._UP = up.bind(this);
    document.addEventListener('mousemove', this._MOVE);
    document.addEventListener('mouseup', this._UP);
};
const move = function move(ev) {
    let curL = ev.pageX - this.startX + this.startL,
        curT = ev.pageY - this.startY + this.startT;
    let minL = 0,
        minT = 0,
        maxL = document.documentElement.clientWidth - this.offsetWidth,
        maxT = document.documentElement.clientHeight - this.offsetHeight;
    curL = curL < minL ? minL : (curL > maxL ? maxL : curL);
    curT = curT < minT ? minT : (curT > maxT ? maxT : curT);
    this.style.left = `${curL}px`;
    this.style.top = `${curT}px`;
};
const up = function up(ev) {
    this.style.cursor = 'default';
    document.removeEventListener('mousemove', this._MOVE);
    document.removeEventListener('mouseup', this._UP);
};
box.addEventListener('mousedown', down);