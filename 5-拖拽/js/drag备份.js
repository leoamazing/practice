/*
 * mousedown:开始拖拽
 *   + 记录 鼠标起始的X/Y轴坐标 && 盒子起始的top/left位置信息
 * mousemove:拖拽中(鼠标移动，盒子也跟着移动)
 *   + 记录 鼠标最新的X/Y轴坐标
 *   + 计算 盒子当前最新的位置 = 鼠标当前位置-鼠标起始位置+盒子起始位置
 * mouseup:拖拽结束
 * -------鼠标按下，触发了mousedown的时候，我们在给当前盒子的mousemove/mouseup事件行为绑定方法「目的：只有鼠标按下，鼠标再移动的时候，盒子才跟着动，如果鼠标不按下，不管怎么移动鼠标，盒子也不动」；鼠标抬起的时候，把之前绑定的方法移除掉；
 */
let box = document.querySelector('#box');

const down = function down(ev) {
    // this->box
    // 记录起始的位置信息 鼠标&&盒子:把信息挂载到当前元素的自定义属性上，这样在其它的方法中，只有获取到这个元素，就可以基于自定义属性获取之前存储的信息了
    this.style.cursor = 'move';
    this.startX = ev.pageX;
    this.startY = ev.pageY;
    this.startL = this.offsetLeft;
    this.startT = this.offsetTop;
    // 解决鼠标焦点丢失问题：新问题「move/up中的this不在是box，而是document了 -> 基于bind预先改变函数中的this指向」
    document.onmousemove = move.bind(this);
    document.onmouseup = up.bind(this);
};
const move = function move(ev) {
    // this->box
    let curL = ev.pageX - this.startX + this.startL,
        curT = ev.pageY - this.startY + this.startT;
    // 边界判断
    let minL = 0,
        minT = 0,
        maxL = document.documentElement.clientWidth - this.offsetWidth,
        maxT = document.documentElement.clientHeight - this.offsetHeight;
    curL = curL < minL ? minL : (curL > maxL ? maxL : curL);
    curT = curT < minT ? minT : (curT > maxT ? maxT : curT);
    this.style.left = `${curL}px`;
    this.style.top = `${curT}px`;
};
const up = function up(ev) {
    // this->box
    this.style.cursor = 'default';
    document.onmousemove = null;
    document.onmouseup = null;
};
box.onmousedown = down;

/*
 * 鼠标焦点丢失问题
 *   + 当前我们是给盒子的mousemove/mouseup事件绑定方法，鼠标只有在盒子上移动或者松开才会触发事件，执行move/up方法
 *   + 鼠标移动过快，鼠标脱离了盒子（焦点丢失）
 *     + 鼠标在盒子外面移动，盒子也不会跟着动
 *     + 鼠标在盒子外面松开，也不会执行up方法，也就导致鼠标松开了，但是mousemove绑定的move方法确没有被移除掉，这样无论鼠标是否按下，只要鼠标再次进入到盒子移动，一样会触发mousemove事件「执行move方法」，导致盒子移动...
 * 
 * 解决方案：
 *   + setCapture / releaseCapture : 让鼠标和盒子绑定在一起(或解绑)
 *     box.setCapture() 按下的时候绑定
 *     box.releaseCapture() 抬起的时候解绑
 *     但是谷歌浏览器不支持
 *   + mousemove和mouseup事件不给盒子进行绑定，绑定给document文档：鼠标跑的再快，也不会脱离文档
 */